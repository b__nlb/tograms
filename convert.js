const UNITS = {
  TABLESPOON: 30,
  TEASPOON: 15,
  CUP: 340
};

function traverse(parentNode, callback) {
  if (!parentNode) return;

  if (parentNode.childNodes.length) {
    for (const node of parentNode.childNodes) {
      traverse(node, callback);
    }
  } else {
    callback(parentNode);
  }
}

function convertText(node) {
  if (node.textContent) {
    node.textContent = node.textContent
      .replace(/(\d+) tablespoon/gi, convertToGrams(UNITS.TABLESPOON))
      .replace(/(\d+) teaspoon/gi, convertToGrams(UNITS.TEASPOON));
  }
}

function convertToGrams(multiplier) {
  return (_, initialValue) => {
    const value = initialValue * multiplier;

    return `${value} gram${value === 1 ? '' : 's'}`;
  };
}

chrome.runtime.onMessage.addListener(message => {
  if (message.action === 'CONVERT') {
    traverse(document.body, convertText);
  }
});

# toGrams

Convert cooking units to gram weights.

## Getting Started

- In Chrome, go here: [chrome://extensions/](chrome://extensions/)
- Click 'Load Unpacked' & select directory containing this repo.

## TODO

- Handle fractions/decimals
- Support more units
- Handle text values instead of digits (three tablespoons vs 3 tablespoons)
- Handle abbreviations such as tbs/TBS
- Update multipliers for common items to be more accurate (e.g., flour vs oil)

let isActive = false;

// Called when the user clicks on the extension button.
chrome.browserAction.onClicked.addListener(tab => {
  const message = {
    action: isActive ? 'UNDO' : 'CONVERT'
  };

  // Send the message to only the active tab.
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, message);
    isActive = !isActive;
  });
});

const initialHtml = document.body.innerHTML;

chrome.runtime.onMessage.addListener(message => {
  if (message.action === 'UNDO') {
    document.body.innerHTML = initialHtml;
  }
});
